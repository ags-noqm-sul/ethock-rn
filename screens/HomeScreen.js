import * as React from 'react';
import { Text, StatusBar, Button, StyleSheet, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SafeAreaView from 'react-native-safe-area-view';
import { WebView } from 'react-native-webview';

let urlChart = "https://www.chartjs.org/samples/latest/charts/bar/vertical.html";

let blueTop = "#363753";
let darkWhiteTop = "#484963";
let mainBgBlue = "#0C153B";
let titleBarColor = "#242644";
let colorTitleDarkWhite = "#FFFFFF"

function HomeScreen({ navigation }) {
    return (
        <SafeAreaView style={[styles.container, { backgroundColor: mainBgBlue }]}>
            <StatusBar barStyle="light-content" backgroundColor="#363753" />
            <Text style={{ color: '#fff' }}>Light Screen</Text>


            <View style={{ height: 200, marginTop: 100 }}>
                <WebView
                    automaticallyAdjustContentInsets={false}
                    source={{ uri: urlChart }}
                />
            </View>

            <Button
                title="Next screen"
                onPress={() => navigation.navigate('Screen2')}
                color="red"
            />
        </SafeAreaView>
    );
}

function Screen2({ navigation }) {
    return (
        <SafeAreaView style={[styles.container, { backgroundColor: mainBgBlue }]}>
            <StatusBar barStyle="light-content" backgroundColor="#363753" />
            <Text style={{ color: '#fff' }}>Light Screen</Text>
            <Button
                title="Next screen"
                onPress={() => navigation.navigate('Home')}
                color="red"
            />
        </SafeAreaView>
    );
}

const Stack = createStackNavigator();

function App() {
    return (

        <Stack.Navigator >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    animationEnabled: false,
                    title: 'My home',
                    headerStyle: {
                        backgroundColor: titleBarColor
                    },
                    headerTintColor: colorTitleDarkWhite,
                }}
            />
            <Stack.Screen
                name="Screen2"
                component={Screen2}
                options={{
                    animationEnabled: false,
                    title: 'My home 2',
                    headerStyle: {
                        backgroundColor: titleBarColor
                    },
                    headerTintColor: colorTitleDarkWhite
                }}
            />
        </Stack.Navigator>
    );
}

export default App;

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
});