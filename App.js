import * as React from 'react';
import { Button, Text, View, Dimensions } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { WebView } from 'react-native-webview';

import s1 from "./screens/s1";
import homeScreen from "./screens/HomeScreen";

import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";


let blueTop = "#363753";
let darkWhiteTop = "#484963";
let mainBgBlue = "#0C153B";
let titleBarColor = "#242644";
let colorTitleDarkWhite = "#FFFFFF";
let mainYellow = "#f1b10f";

let urlChart = "https://www.chartjs.org/samples/latest/charts/bar/vertical.html";

function DetailsScreen() {
    return (
        <View>
            <Text>Bezier Line Chart</Text>
            <LineChart
                data={{
                    labels: ["January", "February", "March", "April", "May", "June"],
                    datasets: [
                        {
                            data: [
                                Math.random() * 100,
                                Math.random() * 100,
                                Math.random() * 100,
                                Math.random() * 100,
                                Math.random() * 100,
                                Math.random() * 100
                            ]
                        }
                    ]
                }}
                width={Dimensions.get("window").width} // from react-native
                height={220}
                yAxisLabel="$"
                yAxisSuffix="k"
                yAxisInterval={1} // optional, defaults to 1
                chartConfig={{
                    backgroundColor: "#e26a00",
                    backgroundGradientFrom: "#fb8c00",
                    backgroundGradientTo: "#ffa726",
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                }}
                bezier
                style={{
                    marginVertical: 8,
                    borderRadius: 16
                }}
            />
        </View>

    );
}

function SettingsScreen({ navigation }) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Settings screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.navigate('Details')}
            />
        </View>
    );
}

const SettingsStack = createStackNavigator();

function SettingsStackScreen() {
    return (
        <SettingsStack.Navigator>
            <SettingsStack.Screen name="Settings" component={SettingsScreen} />
            <SettingsStack.Screen name="Details" component={DetailsScreen} />
        </SettingsStack.Navigator>
    );
}

const Tab = createBottomTabNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Tab.Navigator
                initialRouteName="Ethock"
                tabBarOptions={{
                    activeTintColor: mainYellow,
                    inactiveBackgroundColor: titleBarColor,
                    activeBackgroundColor: titleBarColor,
                    style: {
                        height: 55
                    },
                    labelStyle: {
                        marginBottom: 10,
                        marginTop: 0
                    }

                }}

            >
                <Tab.Screen
                    name="Home"
                    component={homeScreen}
                    options={{
                        tabBarLabel: 'Site List',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="format-align-justify" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Ethock"
                    component={homeScreen}
                    options={{
                        tabBarLabel: 'E-Thock',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="antenna" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Settings"
                    component={SettingsStackScreen}
                    options={{
                        tabBarLabel: 'Notification',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="bell-alert" color={color} size={size} />
                        ),
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    );
}