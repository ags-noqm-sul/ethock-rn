import * as React from 'react';
import { Button, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

let blueTop = "#363753";
let darkWhiteTop = "#484963";
let mainBgBlue = "#0C153B";
let titleBarColor = "#242644";
let colorTitleDarkWhite = "#FFFFFF"

function HomeScreen({ navigation }) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Home screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.navigate('Details')}
            />
        </View>
    );
}

function DetailsScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Details!</Text>
        </View>
    );
}

const HomeStack = createStackNavigator();

function HomeStackScreen() {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen

                name="Home"
                component={HomeScreen}
                options={{
                    animationEnabled: false,
                    title: 'My home',
                    headerStyle: {
                        backgroundColor: titleBarColor
                    },
                    headerTintColor: colorTitleDarkWhite,
                }}

            />
            <HomeStack.Screen name="Details" component={DetailsScreen} />
        </HomeStack.Navigator>
    );
}

export default HomeStackScreen;